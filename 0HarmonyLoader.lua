-- HomebrewHarmony by RhoSigma
HomebrewHarmony = {}

HomebrewHarmony.version = "1.0 15/03/2022"

function HomebrewHarmony:Awake()
    local reflection = Slua.GetClass("System.Reflection.Assembly")
    local path = HBU.GetLuaFolder().."/ModLua/HomebrewHarmony/0Harmony.dll"
    reflection.LoadFrom(path)
end

return HomebrewHarmony
